package ua.dp.krotov.service;

import ua.dp.krotov.model.Category;
import ua.dp.krotov.model.Lot;
import ua.dp.krotov.model.user.User;

import java.util.List;

/**
 * Created by Евгений on 03.06.2016.
 */
public interface LotService {
    void persistLot(Lot lot);
    void deleteLot(Lot lot);
    Lot getLotById (Long id);
    List<Lot> getLots(User user);
    List<Lot> getLots(String productName);
    List<Lot> getLots(Category category);
}
