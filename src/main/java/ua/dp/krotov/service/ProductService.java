package ua.dp.krotov.service;

import ua.dp.krotov.model.Product;

/**
 * Created by E.Krotov on 19.05.2016. (e.krotov@hotmail.com))
 */
public interface ProductService {
    void persistProduct(Product product);
    void deleteProduct(Product product);
    Product getProductById (Long id);
}
