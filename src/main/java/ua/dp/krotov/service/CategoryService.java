package ua.dp.krotov.service;

import ua.dp.krotov.model.Category;

import java.util.List;

/**
 * Created by E.Krotov on 06.05.2016. (e.krotov@hotmail.com))
 */
public interface CategoryService {

    void persistCategory(Category category);
    void deleteCategory(Category category);
    Category getCategoryById (Long id);
    void deleteCategoryById(Long id);
    List<Category> getAllParentCategory();
    List<Category> getAllSubCategory(Category category);
    List<Category> getBreadCrumbsCategory(Category category);
}
