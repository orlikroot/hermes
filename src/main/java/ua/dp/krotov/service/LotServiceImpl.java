package ua.dp.krotov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.krotov.dao.LotDao;
import ua.dp.krotov.model.Category;
import ua.dp.krotov.model.Lot;
import ua.dp.krotov.model.user.User;

import java.util.List;

/**
 * Created by Евгений on 03.06.2016.
 */
@Service("lotService")
@Transactional
public class LotServiceImpl implements LotService {

    @Autowired
    LotDao lotDao;

    @Override
    public void persistLot(Lot lot) {
        lotDao.persistLot(lot);
    }

    @Override
    public void deleteLot(Lot lot) {
        lotDao.deleteLot(lot);
    }

    @Override
    public Lot getLotById(Long id) {
        return lotDao.getLotById(id);
    }

    @Override
    public List<Lot> getLots(User user) {
        return lotDao.getLots(user);
    }

    @Override
    public List<Lot> getLots(String productName) {
        return lotDao.getLots(productName);
    }

    @Override
    public List<Lot> getLots(Category category) {
        return lotDao.getLots(category);
    }
}
