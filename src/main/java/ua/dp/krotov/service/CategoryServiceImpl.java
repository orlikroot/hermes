package ua.dp.krotov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.krotov.dao.CategoryDao;
import ua.dp.krotov.model.Category;

import java.util.List;

/**
 * Created by E.Krotov on 06.05.2016. (e.krotov@hotmail.com))
 */
@Service("categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryDao categoryDao;

    @Override
    public void persistCategory(Category category) {
        categoryDao.persistCategory(category);
    }

    @Override
    public void deleteCategory(Category category) {
        categoryDao.deleteCategory(category);
    }

    @Override
    public Category getCategoryById(Long id) {
        return categoryDao.getCategoryById(id);
    }

    @Override
    public void deleteCategoryById(Long id) {
        categoryDao.deleteCategoryById(id);
    }

    @Override
    public List<Category> getAllParentCategory() {
        return categoryDao.getAllParentCategory();
    }

    @Override
    public List<Category> getAllSubCategory(Category category) {
        return categoryDao.getAllSubCategory(category);
    }

    @Override
    public List<Category> getBreadCrumbsCategory(Category category) {
        return categoryDao.getBreadCrumbsCategory(category);
    }
}
