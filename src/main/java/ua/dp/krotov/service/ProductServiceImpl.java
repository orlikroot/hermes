package ua.dp.krotov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.krotov.dao.ProductDao;
import ua.dp.krotov.model.Product;

/**
 * Created by E.Krotov on 19.05.2016. (e.krotov@hotmail.com))
 */
@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDao productDao;

    @Override
    public void persistProduct(Product product) {
        productDao.persistProduct(product);
    }

    @Override
    public void deleteProduct(Product product) {
        productDao.deleteProduct(product);
    }

    @Override
    public Product getProductById(Long id) {

        return productDao.getProductById(id);
    }
}
