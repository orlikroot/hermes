package ua.dp.krotov.service;

import ua.dp.krotov.model.user.User;

/**
 * Created by Евгений on 03.06.2016.
 */
public interface UserService {
    void persistUser(User user);
    void deleteUser(User user);
    User getUserById (Long id);
}
