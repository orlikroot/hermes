package ua.dp.krotov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.krotov.dao.ImageDao;
import ua.dp.krotov.model.Image;

/**
 * Created by E.Krotov on 29.04.2016. (e.krotov@hotmail.com))
 */
@Service("imageService")
@Transactional
public class ImageServiceImpl implements ImageService {

    @Autowired
    ImageDao imageDao;

    @Override
    public void persistImage(Image image) {
        imageDao.persistImage(image);
    }

    @Override
    public void deleteImage(Image image) {
        imageDao.deleteImage(image);
    }

    @Override
    public Image getImageById(Long id) {
        return imageDao.getImageById(id);
    }
}
