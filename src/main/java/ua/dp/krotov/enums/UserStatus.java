package ua.dp.krotov.enums;

/**
 * Created by E.Krotov on 24.05.2016. (e.krotov@hotmail.com))
 */
public enum UserStatus {
    COMPANY,
    PRIVATE
}
