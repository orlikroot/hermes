package ua.dp.krotov.enums;

/**
 * Created by E.Krotov on 19.05.2016. (e.krotov@hotmail.com))
 */
public enum ProductAvailability {
    IN_STOCK,
    CUSTOM;
}
