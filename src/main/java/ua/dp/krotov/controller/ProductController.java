package ua.dp.krotov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.dp.krotov.model.Product;
import ua.dp.krotov.service.ProductService;

/**
 * Created by E.Krotov on 19.05.2016. (e.krotov@hotmail.com))
 */
@Controller
@RequestMapping(value = "/api/product")
public class ProductController {

    @Autowired
    ProductService productService;


    @RequestMapping(value = "/persist", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    ResponseEntity persistCategory(@RequestBody Product product) {
        productService.persistProduct(product);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity deleteCategory(@RequestBody Product product) {
        productService.deleteProduct(product);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/getproduct", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Product getProductById(@RequestBody Product product) {

        return productService.getProductById(product.getId());
    }
}
