package ua.dp.krotov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.dp.krotov.enums.Period;
import ua.dp.krotov.enums.ProductAvailability;
import ua.dp.krotov.enums.ProductCondition;
import ua.dp.krotov.enums.UserStatus;
import ua.dp.krotov.model.Category;
import ua.dp.krotov.model.Image;
import ua.dp.krotov.model.Lot;
import ua.dp.krotov.model.Product;
import ua.dp.krotov.model.user.UserAddress;
import ua.dp.krotov.model.user.SecurityData;
import ua.dp.krotov.model.user.User;
import ua.dp.krotov.model.user.UserDetails;
import ua.dp.krotov.service.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by E.Krotov on 06.05.2016. (e.krotov@hotmail.com))
 */
@Controller
@RequestMapping(value = "/api/test/fill")
public class FillController {

    @Autowired
    CategoryService categoryService;

    @Autowired
    ProductService productService;

    @Autowired
    ImageService imageService;

    @Autowired
    LotService lotService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/image", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    ResponseEntity persistImage() {
        for (int i=0; i<100; i++) {
            Image image = new Image();
            image.setUrl("http://www.localhost.dp.ua/image" + i + ".jpg");
            imageService.persistImage(image);
        }
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    ResponseEntity persistCategory() {

        for (int i=0; i<10; i++) {
            Category category = new Category();
            category.setName("Parent category " + i);
            categoryService.persistCategory(category);

            for (int y = 0; y < 10; y++) {

                Category subCategory = new Category();
                subCategory.setName("Sub category " + y);
                subCategory.setParent(category);
                categoryService.persistCategory(subCategory);
                for (int z = 0; z < 10; z++) {

                    Category subSubCategory = new Category();
                    subSubCategory.setName("Sub Sub category " + z);
                    subSubCategory.setParent(subCategory);
                    categoryService.persistCategory(subSubCategory);
                }
            }
        }

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/product", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    ResponseEntity persistProduct() {

        for (int i=0; i<100; i++) {

            Product product = new Product();
            product.setName("product " + i);
            product.setPrice(10 * i);
            product.setDescription("Description of product " + i);
            product.setProductCondition(ProductCondition.NEW);
            product.setProductAvailability(ProductAvailability.IN_STOCK);
            product.setCategory(categoryService.getCategoryById(24L));
            List<Image> images = new ArrayList<>();
            images.add(new Image("URL product image " + i));
            images.add(new Image("URL product image " + i*10));
            images.add(new Image("URL product image " + i*100));
           // images.add(imageService.getImageById((long)i+1));
            product.setImages(images);
            productService.persistProduct(product);
        }
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    ResponseEntity persistUser() {
        for (int i=0; i<10; i++) {
           User user = new User();
UserDetails userDetails = new UserDetails();



            userDetails.setFirstName("Evgen");
            userDetails.setLastName("Krotov");
            userDetails.setAbout("I am mega seller");
            userDetails.setNickName("skajeniy_bariga");
            userDetails.setPhone("322-223");
            userDetails.setUserStatus(UserStatus.PRIVATE);

            UserAddress userAddress = new UserAddress();

            userAddress.setCity("Dnipro");
            userAddress.setZip("49000");
            userAddress.setStreet("Darvina str.");
            userAddress.setHouse("1" + i);

            SecurityData securityData = new SecurityData();

            securityData.setEmail("e.krotov@hotmail.com");
            securityData.setPassword("1234567890");
            securityData.setSalt("0987654321");

            user.setUserDetails(userDetails);
            user.setUserAddress(userAddress);
            user.setSecurityData(securityData);
            userService.persistUser(user);

        }
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/lot", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    ResponseEntity persistLot() {
        for (int i=0; i<100; i++) {
            Lot lot = new Lot();
            lot.setPeriod(Period.FIVE);
            lot.setActive(true);
            lot.setProduct(productService.getProductById((long)i+1));
            lot.setQuantity(5);
            lot.setSeller(userService.getUserById(3L));
            lot.setStartDate(new Date());
            lotService.persistLot(lot);
        }
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
    }

