package ua.dp.krotov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.dp.krotov.model.Category;
import ua.dp.krotov.service.CategoryService;

import java.util.List;

/**
 * Created by E.Krotov on 06.05.2016. (e.krotov@hotmail.com))
 */
@Controller
@RequestMapping(value = "/api/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;


    @RequestMapping(value = "/persist", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    ResponseEntity persistCategory(@RequestBody Category category) {
        categoryService.persistCategory(category);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity deleteCategory(@RequestBody Category category) {
        categoryService.deleteCategory(category);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

//    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
//    @ResponseBody
//    public ResponseEntity deleteCategory(@RequestBody String id) {
//        categoryService.deleteCategoryById(Long.parseLong(id));
//        return new ResponseEntity<Void>(HttpStatus.OK);
//    }

    @RequestMapping(value = "/getparents", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<Category> getAllParentCategory() {
        return categoryService.getAllParentCategory();
    }

    @RequestMapping(value = "/subcategories", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<Category> getAllSubCategory(@RequestBody Category category) {
        return categoryService.getAllSubCategory(categoryService.getCategoryById(category.getId()));
    }

    @RequestMapping(value = "/breadcrumbs", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<Category> getBreadCrumbsCategory(@RequestBody Category category) {
        return categoryService.getBreadCrumbsCategory(category);
    }

}
