package ua.dp.krotov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.dp.krotov.model.Image;
import ua.dp.krotov.service.ImageService;

/**
 * Created by E.Krotov on 29.04.2016. (e.krotov@hotmail.com))
 */
@Controller
@RequestMapping(value = "/api/image")
public class ImageController {

    @Autowired
    ImageService imageService;


    @RequestMapping(value = "/persist", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody ResponseEntity persistImage(@RequestBody Image image) {
        imageService.persistImage(image);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity deleteImage(@RequestBody Image image) {
        imageService.deleteImage(image);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
