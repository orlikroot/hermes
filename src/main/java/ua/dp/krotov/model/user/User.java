package ua.dp.krotov.model.user;

import ua.dp.krotov.enums.UserStatus;

import javax.persistence.*;

/**
 * Created by E.Krotov on 24.05.2016. (e.krotov@hotmail.com))
 */
@Entity
@Table(name = "USER")
public class User {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "SECURITY_DATA_ID")
    private SecurityData securityData;


    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "USER_DETAILS_ID")
    private UserDetails userDetails;

    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "USER_ADDRESS_ID")
    private UserAddress userAddress;

    public User(){}

    public User(SecurityData securityData, UserDetails userDetails, UserAddress userAddress) {
        this.securityData = securityData;
        this.userDetails = userDetails;
        this.userAddress = userAddress;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SecurityData getSecurityData() {
        return securityData;
    }

    public void setSecurityData(SecurityData securityData) {
        this.securityData = securityData;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public UserAddress getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(UserAddress userAddress) {
        this.userAddress = userAddress;
    }
}
