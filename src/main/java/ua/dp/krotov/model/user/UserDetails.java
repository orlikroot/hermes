package ua.dp.krotov.model.user;

import ua.dp.krotov.enums.UserStatus;

import javax.persistence.*;

/**
 * Created by E.Krotov on 24.05.2016. (e.krotov@hotmail.com))
 */
@Entity
@Table(name = "USER_DETAILS")
public class UserDetails {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name="USER_STATUS")
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;

    @Column(name = "NICK_NAME")
    private String nickName;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "ABOUT")
    private String about;

    public UserDetails(){}

    public UserDetails(String firstName, String lastName, UserStatus userStatus, String nickName, String phone, String about) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userStatus = userStatus;
        this.nickName = nickName;
        this.phone = phone;
        this.about = about;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
