package ua.dp.krotov.model;

import ua.dp.krotov.enums.ProductAvailability;
import ua.dp.krotov.enums.ProductCondition;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by E.Krotov on 19.05.2016. (e.krotov@hotmail.com))
 */
@Entity
@Table(name = "PRODUCT")
public class Product {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name="PRODUCT_CONDITION")
    @Enumerated(EnumType.STRING)
    private ProductCondition productCondition;

    @Column(name = "PRODUCT_AVAILABILITY")
    @Enumerated(EnumType.STRING)
    private ProductAvailability productAvailability;

    @Column(name = "PRICE")
    private int price;

    @ManyToOne(optional = false)
    @JoinColumn(name="CATEGORY_ID")
    private Category category;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "PRODUCT_IMAGES",
            joinColumns = @JoinColumn(name = "PRODUCT_ID"),
            inverseJoinColumns = @JoinColumn(name = "IMAGE_ID")
    )
     private List<Image> images = new ArrayList<>(10);

    public Product(){super();}

    public Product(String name, String description, ProductCondition productCondition, ProductAvailability productAvailability, int price, Category category, List<Image> images) {
        this.name = name;
        this.description = description;
        this.productCondition = productCondition;
        this.productAvailability = productAvailability;
        this.price = price;
        this.category = category;
        this.images = images;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductCondition getProductCondition() {
        return productCondition;
    }

    public void setProductCondition(ProductCondition productCondition) {
        this.productCondition = productCondition;
    }

    public ProductAvailability getProductAvailability() {
        return productAvailability;
    }

    public void setProductAvailability(ProductAvailability productAvailability) {
        this.productAvailability = productAvailability;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
