package ua.dp.krotov.model;

import javax.persistence.*;

/**
 * Created by Евгений on 05.05.2016.
 */
@Entity
@Table(name = "CATEGORY")
public class Category {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "NAME")
    private String name;

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "PARENT_CATEGORY_ID")
    private Category parent;


    public Category(){}

    public Category(String name, Category parent) {
        this.name = name;
        this.parent = parent;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }
}
