package ua.dp.krotov.model;

import org.hibernate.annotations.Type;
import ua.dp.krotov.enums.Period;
import ua.dp.krotov.model.user.User;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by E.Krotov on 24.05.2016. (e.krotov@hotmail.com))
 */
@Entity
@Table(name = "LOT")
public class Lot {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @Column(name = "START_DATE")
    @Type(type="date")
    private Date startDate;

    @Column(name = "PERIOD")
    @Enumerated(EnumType.ORDINAL)
    private Period period;

    @Column(name = "END_DATE")
    @Type(type="date")
    private Date endDate;

    @JoinColumn(name = "PRODUCT_ID")
    @OneToOne(fetch=FetchType.EAGER)
    private Product product;

    @Column(name = "QUANTITY")
    private int quantity;

    @JoinColumn(name = "SELLER_ID")
    @OneToOne(fetch=FetchType.EAGER)
    private User seller;

    @JoinColumn(name = "BUYER_ID")
    @OneToOne(fetch=FetchType.EAGER)
    private User buyer;

    public Lot(){}

    public Lot(Boolean isActive, Date startDate, Period period, Date endDate, Product product, int quantity, User seller, User buyer) {
        this.isActive = isActive;
        this.startDate = startDate;
        this.period = period;
        this.endDate = endDate;
        this.product = product;
        this.quantity = quantity;
        this.seller = seller;
        this.buyer = buyer;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }
}
