package ua.dp.krotov.dao;

import ua.dp.krotov.model.Product;

/**
 * Created by E.Krotov on 19.05.2016. (e.krotov@hotmail.com))
 */
public interface ProductDao {
    void persistProduct(Product product);
    void deleteProduct(Product product);
    Product getProductById (Long id);
}
