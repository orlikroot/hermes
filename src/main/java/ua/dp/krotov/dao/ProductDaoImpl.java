package ua.dp.krotov.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.dp.krotov.model.Product;

/**
 * Created by E.Krotov on 19.05.2016. (e.krotov@hotmail.com))
 */
@Repository("productDao")
public class ProductDaoImpl extends AbstractDao implements ProductDao {


    @Override
    public void persistProduct(Product product) {
        persist(product);
    }

    @Override
    public void deleteProduct(Product product) {
        delete(product);
    }

    @Override
    public Product getProductById(Long id) {

        Product product = (Product) getSession().get(Product.class, id);
        return product;

//        return (Product) getById(Product.class, id);
    }
}
