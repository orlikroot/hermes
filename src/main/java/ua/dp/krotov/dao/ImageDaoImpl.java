package ua.dp.krotov.dao;

import org.springframework.stereotype.Repository;
import ua.dp.krotov.model.Image;

/**
 * Created by E.Krotov on 29.04.2016. (e.krotov@hotmail.com))
 */
@Repository("imageDao")
public class ImageDaoImpl extends AbstractDao implements ImageDao {
    @Override
    public void persistImage(Image image) {
        persist(image);
    }

    @Override
    public void deleteImage(Image image) {
        delete(image);
    }

    @Override
    public Image getImageById(Long id) {
        return (Image) getById(Image.class, id);
    }
}
