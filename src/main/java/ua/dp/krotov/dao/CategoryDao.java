package ua.dp.krotov.dao;

import ua.dp.krotov.model.Category;

import java.util.List;

/**
 * Created by E.Krotov on 06.05.2016. (e.krotov@hotmail.com))
 */
public interface CategoryDao {
    void persistCategory(Category category);
    void deleteCategory(Category category);
    void deleteCategoryById(Long id);
    Category getCategoryById (Long id);
    List<Category> getAllParentCategory();
    List<Category> getAllSubCategory(Category category);
    List<Category> getBreadCrumbsCategory(Category category);
}
