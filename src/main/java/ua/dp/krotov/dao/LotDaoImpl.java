package ua.dp.krotov.dao;

import org.springframework.stereotype.Repository;
import ua.dp.krotov.model.Category;
import ua.dp.krotov.model.Lot;
import ua.dp.krotov.model.user.User;

import java.util.List;

/**
 * Created by Евгений on 03.06.2016.
 */
@Repository("lotDao")
public class LotDaoImpl extends AbstractDao implements LotDao {
    @Override
    public void persistLot(Lot lot) {
        persist(lot);
    }

    @Override
    public void deleteLot(Lot lot) {
        delete(lot);
    }

    @Override
    public Lot getLotById(Long id) {
        return (Lot) getById(Lot.class, id);
    }

    //TODO implement method
    @Override
    public List<Lot> getLots(User user) {
        return null;
    }

    //TODO implement method
    @Override
    public List<Lot> getLots(String productName) {
        return null;
    }

    //TODO implement method
    @Override
    public List<Lot> getLots(Category category) {
        return null;
    }
}
