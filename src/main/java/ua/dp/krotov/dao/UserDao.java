package ua.dp.krotov.dao;

import ua.dp.krotov.model.user.User;

/**
 * Created by Евгений on 03.06.2016.
 */
public interface UserDao {
    void persistUser(User user);
    void deleteUser(User user);
    User getUserById (Long id);
}
