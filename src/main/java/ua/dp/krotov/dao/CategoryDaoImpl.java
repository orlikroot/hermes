package ua.dp.krotov.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.dp.krotov.model.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by E.Krotov on 06.05.2016. (e.krotov@hotmail.com))
 */
@Repository("categoryDao")
public class CategoryDaoImpl extends AbstractDao implements CategoryDao {



    @Override
    public void persistCategory(Category category) {
        persist(category);
    }

    @Override
    public void deleteCategory(Category category) {
        delete(category);
    }

    @Override
    public void deleteCategoryById(Long id) {
        deleteById(Category.class, id);
    }

    @Override
    public Category getCategoryById(Long id) {

//        Session session = sessionFactory.getCurrentSession();
//        Category category = (Category) session.get(Category.class, id);
//        return category;
        return (Category) getById(Category.class, id);
    }

    @Override
    public List<Category> getAllParentCategory() {
        Query query = getSession().createQuery("FROM Category WHERE parent IS NULL");
        return query.list();
    }

    //Method return List of Category without parent field
    @Override
    public List<Category> getAllSubCategory(Category category) {
        Query query = getSession().createQuery("SELECT id, name FROM Category WHERE parent  = :category");
        query.setParameter("category", category );
        return query.list();
    }

    //TODO  method return redundant information !!!
    @Override
    public List<Category> getBreadCrumbsCategory(Category category) {

        List<Category> categories = new ArrayList<>();

        do  {categories.add(getCategoryById(category.getId()));
            category = getCategoryById(category.getId()).getParent();}
        while(category.getParent() != null);

        return categories;
    }
}
