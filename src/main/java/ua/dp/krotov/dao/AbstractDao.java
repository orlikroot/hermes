package ua.dp.krotov.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ua.dp.krotov.model.Category;

import java.io.Serializable;

/**
 * Created by E.Krotov on 19.04.2016. (e.krotov@hotmail.com))
 */
public abstract class AbstractDao {

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void persist(Object entity) {
        getSession().persist(entity);
    }

    public void delete(Object entity) {
        getSession().delete(entity);
    }

    public Object getById(Class<?> type, Serializable id) {
        return getSession().get(type, id);
    }

    public void deleteById(Class<?> type, Serializable id) {
        Object persistentInstance = getSession().load(type, id);
        if (persistentInstance != null) {
            getSession().delete(persistentInstance);
        }
    }

}
