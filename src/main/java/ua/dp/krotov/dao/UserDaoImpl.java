package ua.dp.krotov.dao;

import org.springframework.stereotype.Repository;
import ua.dp.krotov.model.user.User;

/**
 * Created by Евгений on 03.06.2016.
 */
@Repository("userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {
    @Override
    public void persistUser(User user) {
        persist(user);
    }

    @Override
    public void deleteUser(User user) {
        delete(user);
    }

    @Override
    public User getUserById(Long id) {
        return (User) getById(User.class, id);
    }
}
